from pathlib import Path
from uuid import uuid4

import cryptoshred.app.ui.decrypt as cui
from cryptoshred.entities import container_for


def test_file(static_source_context, capsys):
    cui.file(
        static_source_context,
        Path(__file__).parent.joinpath("../../resources/list_of_containers.json"),
    )
    captured = capsys.readouterr()
    assert "John" in captured.out


def test_list(static_source_context, capsys):
    cui.list(
        static_source_context,
        Path(__file__).parent.joinpath("../../resources/list_of_containers.json"),
    )
    captured = capsys.readouterr()
    assert "John" in captured.out


def test_container(static_source_context, static_backend, capsys):
    id = uuid4()
    cui.container(
        static_source_context,
        container_for(5, id=id, key_backend=static_backend).json(),
    )
    captured = capsys.readouterr()
    assert "\x05" in captured.out

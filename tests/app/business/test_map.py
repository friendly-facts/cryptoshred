from pathlib import Path
from cryptoshred.app.business.map import from_list_in_file


def test_list_in_file(static_backend):
    res = from_list_in_file(
        path=Path(__file__).parent.joinpath("../../resources/list_of_containers.json"),
        key_backend=static_backend,
    )
    assert len(res.keys()) == 3
    assert res["ee789bc0-9a77-4af5-b264-ab7779bd5231"] == ["John"]

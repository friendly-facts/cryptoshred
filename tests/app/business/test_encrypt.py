from uuid import uuid4
import pytest
from cryptoshred.app.business.encrypt import encrypt_value

from cryptoshred.exceptions import KeyNotFoundException


def test_with_non_existing_key(empty_backend):
    with pytest.raises(KeyNotFoundException):
        encrypt_value(value="test", key_backend=empty_backend, sid=uuid4())


def test_without_key(static_backend):
    cc = encrypt_value(value="test", key_backend=static_backend)
    assert cc.value(str) == "test"

import pytest
from base64 import b64decode
from uuid import uuid4

from cryptoshred.engines import AesEngine


def test_crypto_round_trip():
    class StaticBackend:
        def get_key(self, id):
            return (id, b64decode("HrHioKGYe0VNmuLXHXwdL/N+oKSdQ/7uTQFXFKU9b9o="))

    engine = AesEngine(StaticBackend())
    iv = b"nT26prpVGKF5WFdo"
    id = uuid4()
    ct = engine.encrypt(data=b"bob", key_id=id, iv=iv)
    dt = engine.decrypt(cipher_text=ct, key_id=id, iv=iv)
    assert dt.decode("utf-8") == "bob"


def test_error_behavior_on_non_matching_ivs():
    class StaticBackend:
        def get_key(self, id):
            return (id, b64decode("HrHioKGYe0VNmuLXHXwdL/N+oKSdQ/7uTQFXFKU9b9o="))

    engine = AesEngine(StaticBackend())
    iv_encrypt = b"nT26prpVGKF5WFdo"
    iv_decrypt = b"nT26prpVGKF5WFsi"
    id = uuid4()
    ct = engine.encrypt(data=b"bob", key_id=id, iv=iv_encrypt)
    with pytest.raises(ValueError):
        engine.decrypt(cipher_text=ct, key_id=id, iv=iv_decrypt)

from typing import Optional
from uuid import UUID, uuid4, uuid1
import pytest

from pydantic import BaseModel
from cryptoshred.backends import DynamoDbSsmBackend

from cryptoshred.entities import CryptoContainer, container_for


def test_encrypt_new_value_static_backend(static_backend):

    id = uuid4()
    container = container_for(b"James", id=id, key_backend=static_backend)
    stored_value = container.value(bytes)
    assert stored_value == b"James"


def test_encrypt_uuid1(static_backend):

    id = uuid1()
    container = container_for(b"James", id=id, key_backend=static_backend)
    stored_value = container.value(bytes)
    assert stored_value == b"James"


def test_encrypt_int_static_backend(static_backend):

    id = uuid4()
    container = container_for(3, id=id, key_backend=static_backend)
    stored_value = container.value(int)
    assert stored_value == 3


def test_encrypt_new_value_dynamo_backend(cryptoshred_table, iv_ssm, dynamodb):

    container = container_for(
        "Jane",
        key_backend=DynamoDbSsmBackend(
            iv_param="/initialization-vector",
            table_name=cryptoshred_table.table_name,
            dynamo=dynamodb,
        ),
    )
    stored_value = container.value(str)
    assert stored_value == "Jane"


def test_complex_entity(static_backend):
    id = uuid4()

    value = {"first": "value", "second": 2}

    container = container_for(value, id=id, key_backend=static_backend)

    dt = container.value(dict)
    assert dt == value


def test_entity_roundtrip(static_backend):
    class Person(BaseModel):
        first: str
        last: str

    value = Person(first="Jimmy", last="Cook")
    id = uuid4()

    container = container_for(value, id=id, key_backend=static_backend)
    result = container.value(Person)

    assert result == value


def test_nested_use(static_backend):
    class Address(BaseModel):
        line: CryptoContainer[str]
        zip: str

    id = uuid4()
    value = Address(
        line=container_for("some street 14", id=id, key_backend=static_backend),
        zip="a-293749",
    )

    assert value.line.value(str) == "some street 14"


def test_entity_roundtrip_plain(static_backend):
    class Person(BaseModel):
        first: str
        last: str

    value = Person(first="Jimmy", last="Cook")
    id = uuid4()

    container = container_for(value, id=id, key_backend=static_backend)
    result = container.plain()

    assert result == '{"first": "Jimmy", "last": "Cook"}'


def test_complex_usage_example(static_backend):
    class SSN(BaseModel):
        """In the real world this would be a class for a social security number
        with all the checks and validations that go along with that
        """

        number: str

    class Address(BaseModel):
        cc_line: CryptoContainer[str]
        no: int
        zip: int

        @property
        def line(self) -> str:
            if not self.cc_line._key_backend:
                # TODO: Workaround until default backend is solved/general solution exists
                self.cc_line._key_backend = static_backend
            return self.cc_line.value(str) or ""

        @line.setter
        def line(self, new_line: str) -> None:
            self.cc_line = container_for(
                new_line, id=self.cc_line.id, key_backend=static_backend
            )

    class PhoneNumber(BaseModel):
        number: int  # yes storing phone as int is not smart

    class Customer(BaseModel):
        cc_first: CryptoContainer[str]
        cc_ssn: CryptoContainer[SSN]
        cc_address: CryptoContainer[Address]
        cc_phone_no: CryptoContainer[PhoneNumber]
        cc_tracking_id: CryptoContainer[UUID]
        cc_legacy_id: CryptoContainer[int]

        @property
        def first(self) -> str:
            return self.cc_first.value(str) or ""

        @first.setter
        def first(self, new_first: str) -> None:
            self.cc_first = container_for(
                new_first, id=self.cc_first.id, key_backend=static_backend
            )

        @property
        def ssn(self) -> Optional[SSN]:
            return self.cc_ssn.value(SSN)

        @ssn.setter
        def ssn(self, new_ssn: SSN) -> None:
            self.cc_ssn = container_for(
                new_ssn, id=self.cc_ssn.id, key_backend=static_backend
            )

        @property
        def address(self) -> Optional[Address]:
            return self.cc_address.value(Address)

        @address.setter
        def address(self, new_address: Address) -> None:
            self.cc_address = container_for(
                new_address, id=self.cc_address.id, key_backend=static_backend
            )

        @property
        def phone_no(self) -> Optional[PhoneNumber]:
            return self.cc_phone_no.value(PhoneNumber)

        @phone_no.setter
        def phone_no(self, new_phone_no: PhoneNumber) -> None:
            self.cc_phone_no = container_for(
                new_phone_no, id=self.cc_phone_no.id, key_backend=static_backend
            )

        @property
        def tracking_id(self) -> Optional[UUID]:
            return self.cc_tracking_id.value(UUID)

        @tracking_id.setter
        def tracking_id(self, new_tracking_id: UUID) -> None:
            self.cc_tracking_id = container_for(
                new_tracking_id, id=self.cc_tracking_id.id, key_backend=static_backend
            )

        @property
        def legacy_id(self) -> Optional[int]:
            return self.cc_legacy_id.value(int)

        @legacy_id.setter
        def legacy_id(self, new_legacy_id: int) -> None:
            self.cc_legacy_id = container_for(
                new_legacy_id, id=self.cc_legacy_id.id, key_backend=static_backend
            )

    sid = uuid4()
    tracking_id = uuid4()
    customer_dict = {
        "cc_first": container_for("John", id=sid, key_backend=static_backend),
        "cc_ssn": container_for(
            SSN(number="123412"), id=sid, key_backend=static_backend
        ),
        "cc_phone_no": container_for(
            PhoneNumber(number=123837492), id=sid, key_backend=static_backend
        ),
        "cc_tracking_id": container_for(
            tracking_id, id=sid, key_backend=static_backend
        ),
        "cc_legacy_id": container_for(987346, id=sid, key_backend=static_backend),
        "cc_address": container_for(
            Address(
                cc_line=container_for(
                    "Some Street", id=sid, key_backend=static_backend
                ),
                no=3,
                zip=1234,
            ),
            id=sid,
            key_backend=static_backend,
        ),
    }

    customer = Customer(**customer_dict)
    assert customer.ssn.number == "123412"
    assert customer.address.line == "Some Street"
    assert customer.phone_no.number == 123837492
    assert customer.tracking_id == tracking_id
    assert customer.legacy_id == 987346


@pytest.mark.xfail(reason="CC in CC not yet supported")
def test_cc_in_cc(static_backend):
    class Address(BaseModel):
        cc_line: CryptoContainer[str]
        no: int
        zip: int

        @property
        def line(self) -> str:
            return self.cc_line.value(str) or ""

        @line.setter
        def line(self, new_line: str) -> None:
            self.cc_line = container_for(
                new_line, id=self.cc_line.id, key_backend=static_backend
            )

    class Customer(BaseModel):
        cc_address: CryptoContainer[Address]

        @property
        def address(self) -> Optional[Address]:
            return self.cc_address.value(Address)

        @address.setter
        def address(self, new_address: Address) -> None:
            self.cc_address = container_for(
                new_address, id=self.cc_address.id, key_backend=static_backend
            )

    sid = uuid4()
    customer_dict = {
        "cc_address": container_for(
            Address(
                cc_line=container_for(
                    "Some Street", id=sid, key_backend=static_backend
                ),
                no=3,
                zip=1234,
            ),
            id=sid,
            key_backend=static_backend,
        ),
    }
    customer = Customer(**customer_dict)
    assert customer.address.line == "Some Street"

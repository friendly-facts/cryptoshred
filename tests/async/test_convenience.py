import json
import pytest
from cryptoshred.asynchronous.convenience import (
    find_and_decrypt_in_dict,
    find_and_decrypt,
)

"""
These tests are not for real coverage. They are basically just to make sure that
the async translation works. Real coverage is done on the synchronous setup.
"""


@pytest.mark.asyncio
async def test_no_containers(static_backend):
    input = [{"testing": ["nope", {"also": "nope"}]}]
    res = await find_and_decrypt_in_dict(input=input, key_backend=static_backend)
    assert input == res


@pytest.mark.asyncio
async def test_find_and_decrypt_no_containers(static_backend):
    input = json.dumps([{"testing": ["nope", {"also": "nope"}]}])
    res = await find_and_decrypt(x=input, key_backend=static_backend)
    assert input == res

from dataclasses import dataclass
from typing import Dict
import pytest


@dataclass
class Context:
    obj: Dict


@pytest.fixture(scope="session")
def static_source_context(static_backend):
    return Context(obj={"key_backend": static_backend})


@pytest.fixture(scope="session")
def empty_source_context(empty_backend):
    return Context(obj={"key_backend": empty_backend})

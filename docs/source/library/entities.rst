Entities
========

.. contents::
  :local:
  :backlinks: none


Usage with Pydantic
-------------------

See ``tests/test_entities.py::test_complex_usage_example`` for one possible way
to use this lib in combination with Pydantic.

Class Docs
----------

.. autoclass:: cryptoshred.entities.CryptoContainer
  :members:

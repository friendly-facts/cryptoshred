import logging
from uuid import uuid4

from cryptoshred.convenience import find_and_decrypt_in_dict
from cryptoshred.entities import container_for
from tests.fixtures.test_backends import StaticBackend


def test_no_containers(static_backend):
    input = [{"testing": ["nope", {"also": "nope"}]}]
    res = find_and_decrypt_in_dict(input, static_backend)
    assert input == res


def test_no_containers_but_enc(static_backend):
    input = [{"testing": ["nope", {"also": "nope", "enc": "be mean"}]}]
    res = find_and_decrypt_in_dict(input, static_backend)
    assert input == res


def test_value_errors_from_crypto_bubble_up(static_backend, caplog):
    id = uuid4()

    input = [container_for(3, id=id, key_backend=static_backend).dict()]

    backend_with_different_iv = StaticBackend()
    backend_with_different_iv.get_iv = lambda: b"nT26prpVGKF5WFas"

    with caplog.at_level(logging.WARNING):
        find_and_decrypt_in_dict(input, backend_with_different_iv)

    assert "Could not decrypt a container with an unexpected problem" in caplog.text


def test_simple_list(static_backend):
    id = uuid4()

    input = [container_for(3, id=id, key_backend=static_backend).dict()]
    res = find_and_decrypt_in_dict(input=input, key_backend=static_backend)
    assert res == ["\x03"]


def test_mixed_list(static_backend):
    id = uuid4()

    input = [
        container_for(3, id=id, key_backend=static_backend).dict(),
        {"testing": ["nope", {"also": "nope"}]},
    ]
    res = find_and_decrypt_in_dict(input=input, key_backend=static_backend)
    assert res == ["\x03", {"testing": ["nope", {"also": "nope"}]}]


def test_nested_mixed_list(static_backend):
    id = uuid4()

    input = [
        {"key": container_for(3, id=id, key_backend=static_backend).dict()},
        {"testing": ["nope", {"also": "nope"}]},
    ]
    res = find_and_decrypt_in_dict(input=input, key_backend=static_backend)
    assert res == [{"key": "\x03"}, {"testing": ["nope", {"also": "nope"}]}]


def test_nested_list_mixed_list(static_backend):
    id = uuid4()

    input = [
        [{"key": container_for(3, id=id, key_backend=static_backend).dict()}, "value"],
        {"testing": ["nope", {"also": "nope"}]},
    ]
    res = find_and_decrypt_in_dict(input=input, key_backend=static_backend)
    assert res == [[{"key": "\x03"}, "value"], {"testing": ["nope", {"also": "nope"}]}]


def test_json_value_deserialized_correctly(static_backend):
    id = uuid4()

    input = [
        [
            {
                "key": container_for(
                    {"inner_key": "inner_value"}, id=id, key_backend=static_backend
                ).dict()
            },
            "value",
        ]
    ]
    res = find_and_decrypt_in_dict(input=input, key_backend=static_backend)
    assert res == [[{"key": {"inner_key": "inner_value"}}, "value"]]


def test_list_deserialized_correctly(static_backend):
    id = uuid4()

    input = [
        [
            {"key": container_for([1, 2, 3], id=id, key_backend=static_backend).dict()},
            "value",
        ]
    ]
    res = find_and_decrypt_in_dict(input=input, key_backend=static_backend)
    assert res == [[{"key": [1, 2, 3]}, "value"]]

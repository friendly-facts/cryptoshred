# Cryptoshred

Welcome to cryptoshred. You can find more extensive documentation over at [readthedocs](https://cryptoshred.readthedocs.io/en/latest/).

This project arose manly out of the necessity to work with events that contain cryptoshredded information.
It is an implementation of cryptoshredding that is compatible with [this Java implementation](https://github.com/prisma-capacity/cryptoshred). It can either be used independently or together with [pyfactcast](https://pypi.org/project/pyfactcast/).

Contributions are welcome. Just get in touch.

## Quickstart

Simply `pip install cryptoshred` and get going. The cli is available as `cryptoshred` and
you can run `cryptoshred --help` to get up to speed on what you can do.

## Development

This project uses `poetry` for dependency management and `pre-commit` for local checks.

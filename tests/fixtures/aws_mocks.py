import pytest
import boto3
import os
from moto import mock_dynamodb2, mock_ssm


@pytest.fixture(scope="session", autouse=True)
def boto_region():
    boto3.setup_default_session(region_name="eu-central-1")


@pytest.fixture(scope="session", autouse=True)
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"
    os.environ["AWS_DEFAULT_REGION"] = "us-east-1"


@pytest.fixture(scope="session")
def dynamodb(aws_credentials):
    with mock_dynamodb2():
        yield boto3.resource("dynamodb", region_name="us-east-1")


@pytest.fixture(scope="session")
def ssm(aws_credentials):
    with mock_ssm():
        yield boto3.client("ssm", region_name="us-east-1")


@pytest.fixture(scope="function")
def cryptoshred_table(dynamodb):
    table = dynamodb.create_table(
        TableName="cryptoshred-keys-test",
        KeySchema=[{"AttributeName": "subjectId", "KeyType": "HASH"}],
        AttributeDefinitions=[{"AttributeName": "subjectId", "AttributeType": "S"}],
    )

    yield table

    table.delete()


@pytest.fixture(scope="module")
def iv_ssm(ssm):
    ssm.put_parameter(Name="/initialization-vector", Value="76ZbtzkUA5s3O0AU")

    yield

    ssm.delete_parameters(
        Names=[
            "/initialization-vector",
        ]
    )

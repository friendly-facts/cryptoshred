import pytest


@pytest.fixture(scope="session")
def jimmy_container():
    return {
        "id": "d9be41a2-f34b-43ab-a7cc-6a6cd3034901",
        "enc": "KSVqeLQygDMeMXcQgLAyCTwKub3IWitB8t3C3Po4s8Z9LqTsXorhq9nJYL0kO93Q",
        "algo": "AES",
        "ksize": 256,
    }

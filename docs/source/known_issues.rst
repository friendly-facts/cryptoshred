Known Issues
============


CLI Deserialized Datatype
-------------------------

Currently the CLI will deserialize every value as string regardless of the actual
datatype this is less than ideal especially given that binary to datatype is of course
not a clear cut thing. However I consider it a reasonable default for the moment until I find the
time to make this more magic see `this ticket <https://gitlab.com/edthamm/cryptoshred/-/issues/5>`_ .

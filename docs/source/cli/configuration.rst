Configuration
=============

As a general rule, the environment always overwrites the configuration from file.
I.e. if you set the table name in your config file and your environment, the
value from the environment will be used. All other values will be used from the
configuration file though.

This way you can:

- quickly overwrite some settings for testing local to your shell
- decide on where and how to keep potentially sensitive information
- mix and match variables as per your requirements


Through the Environment
-----------------------

The following

- ``DYNAMO_BACKEND_IV_PARAM`` (required): The SSM parameter that holds your initialization vector
- ``DYNAMO_BACKEND_TABLE_NAME``: The name of the table holding the keys


Through the Configuration File
------------------------------

Create a directory called ``.cryptoshred`` in your users home directory. In this
directory create configuration files named after your environment with a file ending of env.
For example ``~/.cryptoshred/production.env`` would allow you to use the production profile like some
``cryptoshred -p production ...``.

If you specify no profile in your invocation, cryptoshred will look for the ``default.env`` file and use
what is configured in there.

The syntax of the ``.env`` files is simple. Here is an example:


.. code:: shell

  # this is a comment
  DYNAMO_BACKEND_IV_PARAM=/my/value


For those that want more detail, pydantic is used to manage the settings.

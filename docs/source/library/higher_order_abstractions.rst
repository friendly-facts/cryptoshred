Higher Order Abstractions
=========================

.. contents::
  :local:
  :backlinks: none

.. autofunction:: cryptoshred.entities.container_for
.. autofunction:: cryptoshred.entities.container_for_str
.. autofunction:: cryptoshred.entities.container_for_bytes
.. autofunction:: cryptoshred.entities.container_for_base_model
.. autofunction:: cryptoshred.entities.container_for_dict
.. autofunction:: cryptoshred.entities.container_for_int

.. autofunction:: cryptoshred.convenience.find_and_decrypt_in_dict

.. Cryptoshred documentation master file, created by
   sphinx-quickstart on Thu May 13 10:29:57 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root ``toctree`` directive.

Welcome to Cryptoshred's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   cli/index
   library/index
   known_issues
   faq/index




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

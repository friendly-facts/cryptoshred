Mapping
=======

Map Subject ID to PII
---------------------

If you want to use ``map list`` to construct a map of subject ID to PII, your best bet at the moment
is to extract a list of crypto containers from your event stream. This can be done using ``jq``.
If for example your payload contains crypto containers at the top level, this statement:

.. code:: shell

  jq '[.[].payload[]]|map(objects)|map(select(.enc))'

would extract all crypto containers into a list so you could do the following.

.. code:: shell

  factcast streams subscribe customers --json > customers.json
  cat customers.json | jq '[.[].payload[]]|map(objects)|map(select(.enc))' > customer_containers.json
  cryptoshred map list customer_containers.json

Which will yield a list of ID <> PII mappings.

Of course you can use ``jq`` for deeper nestings as well.
